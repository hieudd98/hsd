# Hate Speech Detection

#### Abstract
In recent years, the increasing propagation of hate speech on social media and the urgent need for effective countermeasures have drawn significant investment from governments, companies, and researchers. A large number of methods have been developed for automated hate speech detection online. This aims to classify textual content into non-hate or hate speech, in which case the method may also identify the targeting characteristics (i.e., types of hate, such as race, and religion) in the hate speech.

Keywords: hate speech, classification, neural network, CNN, GRU, skipped CNN, deep learning, natural language processing

## **1. Handle Imbalanced Data**
### **1.1 Sampling**
  * Upsampling
  * Downsampling
### **1.2 Synthetic data generation (Data augmentation)**
### **1.3 Adjust performance metrics/loss**
  * Weight balancing
    * class_weight
    * focal loss
### **1.4 Use cost-sensitive algorithm (boosting)**

## **2. Approaches**
### **2.1 TF-IDF + SVM**

### **2.2 Word Representation + Classifier**
  * Word embeddings algorithms *word2vec* and *GloVe* provide a mapping of words to a high-dimensional continuous vector space where different words with a similar meaning have a similar vector representation. These word embeddings, pre-trained on large amounts of unlabeled data, are used to initialize the first layer of a neural network called embedding layer, the rest of the model is then trained on data of a particular task.
  * Example architecture:  
    $\hspace{0.5cm}$ Text (X) $\to$ Embedding $\to$ Deep Network (LSTM/GRU) $\to$ Fully Connected $\to$ Output Layer (Sigmoid, Softmax) $\to$ Predicted Class (Y)

### **2.3 Text Classification using Language Models**
  * Approach:
    * Data pre-processing
    * Train or use exsisting pretrained language model
    * Build classifier with 2 layers which gets output of the encoder of language model as input
  * Intuition: We can observe a remarkable simiarity between Naive Bayes and the general language modeling approach in Infomation Retrieval (IR): the general probbilistic framework is the same, and both use smoothing to solve the zero-probability problem.

#### ULMFiT:
  * ULMFiT is a Transfer Learning method that can be applied to any task in NLP
  * ULMFiT uses AWD-LSTM (Merity et al. 2017a) which was sota Language Model in 2018
  * This single architecture is used thoughout - for per-training as well as for fine-tuning
  * There are three stages in ULMFiT:
    * General domain language model pre-training
    * Target task language model fine-tuning
    * Target task classifier fine-tuning

#### 2.3.1 General domain Language model Pre-training
  * This is analogous to the ImageNet pretrained in computer vision
  * Language model can capture general properties of a language
#### 2.3.2 Target task language model fine-tuning
  In this step, the language model is fine-tuned on data from the target task. This step improves the classification model on small datasets. These following methods are applied:
  * **Slanted Triangular Learning Rates (STLR)**
    * The learning rate first increases linearly and then decays linearly. STLR is a modification of the *triangular learning rate (Smith 2017)* with a short increase and a long decay period
    * **Intuition**: An initial short increase is for quickly convergence. This is followed by a long decay period which allows for the further refining of the paremeters  
      $cut = \lfloor T \cdot cut\_frac \rfloor$  
      $p = \begin{cases}
        t/{cut\_frac} & \text{if } t < cut \\
        1 - \dfrac{t-cut}{cut \cdot (1/cut\_frac-1)} & \text{otherwise}
      \end{cases}$  
      $\eta_t = \eta_{max} \cdot \dfrac{1 + p \cdot (ratio - 1)}{ratio}$
      * **T** is the number of training iterations
      * **cut_frac** is the fraction of iterations we increase the LR
      * **cut** is the iteration when we switch from increasing to decreasing the LR
      * for **t < cut, p** is the number of iterations the LR has increased upon the total number of increasing iterations and, for **t >= cut, p** is the total number of iterations the LR has decreased upon the total number of decreasing iterations
      * **ratio** specifies how much smaller the lowest LR is from the maximum LR, **$\eta_\text{max}$**
      * generally, **cut_frac** = 0.1, ratio = 32 and $\eta_\text{max} = 0.01$
  * **Discriminative Fine-Tuning**:
    * Different layers in a model capture different types of information, and hence require different learning rate
    * We first chooses the learning rate of the last layer by fine-tuning only the last layer and uses following formla for lower layers: $\eta^{l-1} = \eta^l /_{2.6}$
#### 2.3.3 Target task classifier fine-tuning
  The final stage involves two additional linear blocks: **ReLU** activation for intermediate layer and **softmax** for the final linear layer. Each block uses batch normalization and dropout.
  * **Concat Pooling**
    * The first linear layer takes as input the pooled last hidden layer states. The signal in a text classification task is often contained in a few words, which may occur anywhere in the document. Hence, information may get lost if we only consider the last hidden state of the model. Therefore, the last hidden state of the model is concatenated with the max-pooled and the mean-pooled representations of the hidden states over as many time step as fit in the GPU memory.
  * **Gradual Unfreezing**
    * The target classifier fine-tuning is very sensitive and an aggressive fine-tuning may lead to nullifying the benefits of language model pre-training
    * Propose Gradual unfreezing:
      * The last LSTM layer is first unfrozen and the model is fine-tuned for one epoch
      * Then the next lower frozen layer is unfrozen
      * This process is repeated until all layers are fine-tuned to convergence
  * **BackPropagation Through Time for Text Classification (BPT3C)**
    * The document is divided into fixed-length chunks
    * At the beginning of each chunk, the model is initialized with the final state of the previous chunk.
    * Gradients are back propagated to the batches whose hidden state contributed to the final prediction
    * In practice, variable length backpropagation sequences are used
  * **Bidirectional Language model**
    * Both, a forward and backward language model are pretrained
    * The classifier is then fine-tuned for both the language models independently
    * The average of the two classifier predictions is taken as the final output




### ** Embedding + Max pooling**
### ** Embedding + Average pooling**
### ** CNN**
### ** RNN**
### ** Memory Net**

## Key notes
  * Gradient Boosted Decision Trees (GBDTs) significantly increases score: ![comparison](images/comparison.png)